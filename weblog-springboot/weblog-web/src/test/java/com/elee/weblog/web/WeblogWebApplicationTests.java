package com.elee.weblog.web;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
class WeblogWebApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testLog(){
        log.info("This is a test log");
        log.warn("This is a warning log");
        log.error("This is an error log");

        //占位符
        String author = "lee";
        log.info("Author {} wrote a blog.", author);
    }

}
